'use strict';

angular.module('amt.jsonEditor', [])
    .provider('jsoneditor', function () {
        function extend(Child, Parent) {
            var F = function () {
            };
            F.prototype = Parent.prototype;
            Child.prototype = new F();
            Child.prototype.constructor = Child;
            Child.superclass = Parent.prototype;
        }

        function JSONEditorAMT(container, options, json) {

            JSONEditorAMT.superclass.constructor.call(this, container, options, json);

            this.get_node_path = function (node, right_path) {
                var left_path = (node.field ? node.field : (node.index != undefined ? node.index : "root"));
                right_path = left_path + "." + right_path;
                if (node.parent != undefined) {
                    return this.get_node_path(node.parent, right_path)
                }
                if (right_path[right_path.length - 1] == ".") {
                    return right_path.slice(0, -1)
                }
                return right_path
            };
            this.get_edit_log = function () {
                if (this.history == undefined) {
                    return {}
                }
                var log = [];
                var last_index = log.length - 1;
                var last_event = log[last_index];
                var path = undefined;
                var self = this;
                _.forEach(this.history.history, function (obj) {
                    last_index = log.length - 1;
                    if (last_index > -1)
                        last_event = log[last_index];
                    else
                        last_event = {action: null, path: null};
                    path = self.get_node_path(obj.params.node, "");
                    switch (obj.action) {
                        case "editField":
                        case "editValue":
                            if (last_event.action == obj.action && last_event.path == path) {
                                last_event.new = obj.params.newValue;
                                log[last_index] = last_event
                            } else {
                                log.push({
                                    action: obj.action,
                                    path: path,
                                    old: obj.params.oldValue,
                                    new: obj.params.newValue
                                })
                            }
                            break;
                        case "changeType":
                            if (last_event.action == obj.action && last_event.path == path) {
                                last_event.new = obj.params.newType;
                                log[last_index] = last_event
                            } else {
                                log.push({
                                    action: obj.action,
                                    path: path,
                                    old: obj.params.oldType,
                                    new: obj.params.newType
                                })
                            }
                            break;
                        case "removeNode":
                            var type = obj.params.node.type;
                            var old_value = type == "auto" || type == "string"
                                ? obj.params.node.value
                                : "";
                            log.push({ action: obj.action, path: path, old: old_value});
                            break;
                        case "duplicateNode":
                            log.push({ action: obj.action, path: path});
                            break;
                        case "moveNode":
                            log.push({ action: obj.action, path: path,
                                old: obj.params.startParent != obj.params.endParent
                                    ? self.get_node_path(obj.params.startParent, obj.params.startIndex)
                                    : obj.params.startIndex,
                                new: obj.params.startParent != obj.params.endParent
                                    ? self.get_node_path(obj.params.endParent, obj.params.endIndex)
                                    : obj.params.endIndex });
                            break;
                    }
                });
                return log
            }
        }

        extend(JSONEditorAMT, jsoneditor.JSONEditor);
        this.$get = function () {
            return JSONEditorAMT
        }
    });